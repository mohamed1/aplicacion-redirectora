#!/usr/bin/python

#
# Simple HTTP Server with Redirection
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# September 2010
# September 2009
# Febraury 2022


import socket
import random


urls = ["https://www.google.com/", "https://www.youtube.com/","https://labs.etsit.urjc.es/",
        "https://www.aulavirtual.urjc.es/","https://gitlab.etsit.urjc.es/"]

def redireccion():
    url_seleccionada = random.randint(0, len(urls) - 1)

    return "HTTP/1.1 301 Moved Permanently\r\nLocation: " + urls[url_seleccionada] + "\r\n\r\n"


# Create a TCP object socket and bind it to a port
# We bind to 'localhost', therefore only accepts connections from the
# same machine
# Port should be 80, but since it needs root privileges,
# let's use one above 1024

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as mySocket:
    mySocket.bind(('', 1250))

    # Queue a maximum of 5 TCP connection requests
    mySocket.listen(5)

    # Accept connections, read incoming data, and answer back an HTLM page
    #  (in a loop)
    while True:
        print("Waiting for connections")
        (recvSocket, address) = mySocket.accept()
        with recvSocket:
            print("HTTP request received:")
            data = recvSocket.recv(2048).decode("utf-8")
            print(data)
            response = redireccion()
            recvSocket.send(response.encode('ascii'))
